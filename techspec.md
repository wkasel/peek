# Peek Architecture overview
## Data Model

== channel ==
- first_name
- last_name
- channel_name
- bio
- avatar
- header_image
- tags
- location
- follower_count
- subscriber_count
- last_active
- twitter_user
- youtube
- facebook
- instagram
- url
- discord
- is_adult


== channel_stream ==
- channel_id
- name
- price
- tags
- is_private
- is_story_stream

== channel_stream_content_item ==
- is_public
- is_premium
- price
- s3_info
- total_views
- total_tips
- liked_by

== channel_stream_content_item_comment ==
- user_id
- content_piece_id
- comment
- reply_to
- created_at

== channel_stories ==
- channel_id
- channel_stream_id
- created_at
- expires_at (note, expiration doesn't delete - just hides)


== live_session ==
> Something to track live sessions. Need to consider that live sessions can be free, one-time-payment, accessed as part of a stream, and also pay-per-minute
- channel_id
- channel_stream_id
- price
- is_scheduled
- max_capacity (?)

== payment_transaction ==
- user_id
- channel_id
- stream_id
- content_piece_id
- transaction_time
- ip_address
- geo
- (payment gateway metadata)
- payment_type (add_to_wallet,per_minute_topup, a_la_carte, recurring_stream)

== user ==
- first_name
- last_name
- email
- public_profile (we allow the user to opt out of a public profile)
- connected_social_auth
- verified (I think we only will do this for creators, so may not need)
- settings
- tags
- subscribed_streams
- purchases
- is_online


== user_purchases ==
- content_id
- stream_id
- is_active
- transaction_id
- status

== user_feeds ==
> (this should be like multis in reddit)
- user_id
- feed_name
- is_custom
- includes (an array of subscribed streams to combine).
- is_public (is this visable on their profile?)
- get_stream_metadata (primary key linking this back to getstream.io api)

== user_notifications ==
> (need to think through this more, but most of this is handled via getstream)
- user_id
- notification
- notification_type
- notification_content
- action_item
- get_stream_metadata (primary key linking it back to getstream.io api)


== global_hashtags ==
- id
- name
- related_tags (array of other tags to associate)
- is_adult

== audit_log ==
- user_id
- action_taken
- ip_address
- geo_address
- created_at
- content
> [some actions to be included]
- agreed_to_terms
- payout_initiated
- contacted_support


== messaging ==
> powered via getStream.io
- channel_id
- user_id
- message

== customer_support ==
> intercom support
- intercom_ticket_key
- action_log
- user_id

== super_administration == 
> ForestAdmin can handle most of what we need here.
- IP Blacklisting
- User Blocking




## External Services ##
- messaging (getStream)
- streams/feeds (getStream)
- search (algolia)
- outbound mail service (mailchimp)
- video on demand - Amplify Video
- video streaming - Amplify Video
- payments (stripe, chargify, rocketgateway) (inbound & outbound)
- creator/user verification (https://withpersona.com)
- personalization / recommendations (https://aws.amazon.com/blogs/machine-learning/creating-a-recommendation-engine-using-amazon-personalize/)[amazon personalize]
- NFT service (need to find open source provider)

## Helpful Libraries
- https://github.com/videojs/http-streaming


## Technical Notes
- For all video delivery we need to ensure we use Amplify's built in VOD service. (can we use this for photos too?)
- We should proxy & cache getstream's api - in my experience it can be very slow.
- allow creators to enable 2FA
- consider running _all multimedia_ through Lambda@Edge. Later we could persist sessions with redis.

## Helpful Resources
- https://stackoverflow.com/questions/64198602/how-to-secure-hls-streaming-using-aws-for-mobile-devices

## Future Development
- NFT Integration - how do we support it easily?
- Referral program. OF does a MLM style earnings situation. (potential app https://www.talon.one)
- Gif integration inside chat
- content activity rating (alert creators who aren't posting enough content, and provide an activity score for the end-user to see)
- Watermark images