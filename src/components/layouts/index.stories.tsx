import React from "react";
import {
  Container,
  ContentWithPaddingXl,
  Content2Xl,
  ContentWithVerticalPadding,
  ContentWithPaddingLg,
} from ".";

export default {
  title: "Layouts",
  component: Container,
};
export const _Container = () => <Container>I'm inside the container</Container>;
export const _ContentWithPaddingXl = () => (
  <ContentWithPaddingXl>I'm inside the container</ContentWithPaddingXl>
);
export const _Content2Xl = () => <Content2Xl>I'm inside the container</Content2Xl>;
export const _ContentWithVerticalPadding = () => (
  <ContentWithVerticalPadding>I'm inside the container</ContentWithVerticalPadding>
);
export const _ContentWithPaddingLg = () => (
  <ContentWithPaddingLg>I'm inside the container</ContentWithPaddingLg>
);
