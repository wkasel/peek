import { store } from "src/services/feedback";
import { useSnapshot } from "valtio";

// This component is wired up to the application layout.
//
// To launch an alert, modal, or other transient component,
// pass the JSX component directly into the feedback store.
//
export const Feedback = () => {
  const feedback = useSnapshot(store);

  const Alert = feedback.alert?.type;
  const children = feedback.alert?.props.children;

  return <>{feedback.alert && <Alert>{children}</Alert>}</>;
};
