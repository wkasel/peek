import React from "react";
import { Success } from "./Success";
import { Error } from "./Error";

export default {
  title: "Alert",
  component: Success,
};

export const _Success = () => <Success>I'm a success alert</Success>;
export const _Error = () => <Error>I'm a error alert</Error>;
