import React from "react";
import { LavaLamp } from "./LavaLamp";

export default {
  title: "LavaLamp",
  component: LavaLamp,
};

const palette = [
  [
    [198, 89, 198],
    [37, 159, 222],
  ],
  [
    [0, 159, 255],
    [151, 0, 255],
  ],
];

export const Default = () => (
  <LavaLamp
    width={800}
    height={600}
    palette={palette}
    speed={2}
    scale={100}
    resolution={50}
    fadeInTime={150}
    animate={true}
  />
);
