import React from "react";
import { Button, PrimaryButton } from ".";
export default {
  title: "Button",
  component: Button,
};

export const Default = (args) => <Button {...args}>Default button</Button>;
Default.args = { className: "rounded-sm" };

export const Primary = () => <PrimaryButton>I'm a button</PrimaryButton>;
Primary.args = {};
