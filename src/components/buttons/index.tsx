import React from "react";

import tw from "twin.macro";
export type IButton = React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
>;

export const PrimaryButton = tw.button`px-8 py-3 font-bold rounded-lg bg-primary-500 text-gray-100 hocus:bg-primary-700 hocus:text-gray-200 focus:shadow-outline focus:outline-none transition-all duration-300`;

export const Button: React.FC<IButton> = ({ className = "", children, ...rest }) => {
  return (
    <button
      className={`py-2 px-4 rounded bg-green-500 hover:bg-green-600 focus:outline-none ring-opacity-75 ring-green-400 focus:ring text-white text-lg ${className}`}
      {...rest}
      data-testid="btn"
    >
      {children}
    </button>
  );
};
