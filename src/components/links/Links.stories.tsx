import React from "react";
import { PrimaryLink } from "./Links";

export default {
  title: "Link",
  component: PrimaryLink,
};

export const Primary = () => <PrimaryLink>I'm a link</PrimaryLink>;
