import React from "react";
import tw from "twin.macro";
import { signIn, store } from "src/services/auth";
import { useRouter } from "next/router";
import { useSnapshot } from "valtio";
import Link from "next/link";
import MemoLoading from "../icons/Loading";
import { Logo } from "../logo";

const Content = tw.div`max-w-screen-xl m-0 sm:mx-20 sm:my-16 bg-white text-gray-900 shadow-md sm:rounded-lg flex justify-center flex-1`;
const MainContainer = tw.div`lg:w-1/2 xl:w-5/12 p-6 sm:p-12`;
const MainContent = tw.div`mt-12 flex flex-col items-center`;
const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold`;
const FormContainer = tw.div`w-full flex-1 mt-8`;
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border-0 border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const IllustrationContainer = tw.div`sm:rounded-r-lg flex-1 bg-purple-100 text-center hidden lg:flex justify-center`;
const SubmitButton = tw.button`min-h-16 mt-5 tracking-wide font-semibold bg-primary-500 text-gray-100 w-full py-4 rounded-lg hover:bg-primary-900 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none`;

export const Login = ({
  logoLinkUrl = "/",
  headingText = "Sign In To Peek",
  submitButtonText = "Sign In",
  forgotPasswordUrl = "forgot-password",
  signupUrl = "register",
}) => {
  const router = useRouter();
  const auth = useSnapshot(store);
  return (
    <Content>
      <MainContainer>
        <Link href={logoLinkUrl}>
          <a className="w-full">
            <Logo />
          </a>
        </Link>
        <MainContent>
          <Heading>{headingText}</Heading>
          <FormContainer>
            <Form
              onSubmit={async (e) => {
                e.preventDefault();
                await signIn({
                  email: e.target[0].value,
                  password: e.target[1].value,
                });
                router.push("/dashboard");
              }}
            >
              <Input type="email" placeholder="Email" />
              <Input type="password" placeholder="Password" />
              <SubmitButton type="submit">
                {auth.loading ? <MemoLoading /> : <span className="text">{submitButtonText}</span>}
              </SubmitButton>
            </Form>
            <p tw="mt-6 text-xs text-gray-600 text-center">
              <Link href={forgotPasswordUrl} tw="border-b-0 border-gray-500 border-dotted">
                Forgot your password?
              </Link>
            </p>
            <p tw="mt-1 text-sm text-gray-600 text-center">
              Dont have an account?{" "}
              <Link href={signupUrl} tw="border-b-0 border-gray-500 border-dotted">
                Sign Up
              </Link>
            </p>
          </FormContainer>
        </MainContent>
      </MainContainer>
      <IllustrationContainer></IllustrationContainer>
    </Content>
  );
};
