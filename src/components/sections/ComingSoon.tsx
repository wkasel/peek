import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { LavaLamp } from "../lavalamp";
import LogoIcon from "../icons/Logo";

const palette = [
  [
    [198, 89, 198],
    [37, 159, 222],
  ],
  [
    [0, 159, 255],
    [151, 0, 255],
  ],
];

const Content: React.FC = () => {
  const router = useRouter();
  return (
    <div className="relative overflow-hidden">
      <div
        className="hidden sm:block sm:absolute sm:inset-y-0 sm:h-full sm:w-full"
        aria-hidden="true"
      >
        <div className="relative h-full max-w-7xl mx-auto">
          {/* <svg className="absolute right-full transform translate-y-1/4 translate-x-1/4 lg:translate-x-1/2" width={404} height={784} fill="none" viewBox="0 0 404 784">
          <defs>
            <pattern id="f210dbf6-a58d-4871-961e-36d5016a0f49" x={0} y={0} width={20} height={20} patternUnits="userSpaceOnUse">
              <rect x={0} y={0} width={4} height={4} className="text-gray-200" fill="currentColor" />
            </pattern>
          </defs>
          <rect width={404} height={784} fill="url(#f210dbf6-a58d-4871-961e-36d5016a0f49)" />
        </svg> */}
        </div>
      </div>
      <div className="relative pt-6 pb-16 sm:pb-24">
        <div className="max-w-7xl mx-auto px-4 sm:px-6">
          <nav
            className="relative flex items-center justify-between sm:h-10 md:justify-center"
            aria-label="Global"
          >
            <div className="flex items-center flex-1 md:absolute md:inset-y-0 md:left-0">
              <div className="flex items-center justify-between w-full md:w-auto">
                <a href="#">
                  <span className="sr-only">Workflow</span>
                </a>
                <div className="-mr-2 flex items-center md:hidden">
                  <button
                    type="button"
                    className="bg-gray-50 rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
                    aria-expanded="false"
                  >
                    <span className="sr-only">Open main menu</span>
                    {/* Heroicon name: outline/menu */}
                    {/* <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
                  </svg> */}
                  </button>
                </div>
              </div>
            </div>
            {/* <div className="hidden md:flex md:space-x-10">
            <a href="#" className="font-medium text-gray-500 hover:text-white">Product</a>
            <a href="#" className="font-medium text-gray-500 hover:text-white">Features</a>
            <a href="#" className="font-medium text-gray-500 hover:text-white">Marketplace</a>
            <a href="#" className="font-medium text-gray-500 hover:text-white">Company</a>
          </div> */}
            <div className="hidden md:absolute md:flex md:items-center md:justify-end md:inset-y-0 md:right-0">
              <span className="inline-flex rounded-md shadow">
                <Link href="login">
                  <a className="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white hover:bg-gray-50">
                    Log in
                  </a>
                </Link>
              </span>
            </div>
          </nav>
        </div>

        <div className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden">
          <div className="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
            <div className="px-5 pt-4 flex items-center justify-between">
              <div>
                <LogoIcon stroke="#eee" />
              </div>
              <div className="-mr-2">
                <button
                  type="button"
                  className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
                >
                  <span className="sr-only">Close menu</span>
                  {/* Heroicon name: outline/x */}
                  {/* <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                </svg> */}
                </button>
              </div>
            </div>
            <div className="px-2 pt-2 pb-3">
              <a
                href="#"
                className="block px-3 py-2 rounded-md text-base font-medium text-white hover:text-white hover:bg-gray-50"
              >
                Product
              </a>
              <a
                href="#"
                className="block px-3 py-2 rounded-md text-base font-medium text-white hover:text-white hover:bg-gray-50"
              >
                Features
              </a>
              <a
                href="#"
                className="block px-3 py-2 rounded-md text-base font-medium text-white hover:text-white hover:bg-gray-50"
              >
                Marketplace
              </a>
              <a
                href="#"
                className="block px-3 py-2 rounded-md text-base font-medium text-white hover:text-white hover:bg-gray-50"
              >
                Company
              </a>
            </div>
            <a
              href="#"
              className="block w-full px-5 py-3 text-center font-medium text-indigo-600 bg-gray-50 hover:bg-gray-100"
            >
              Log in
            </a>
          </div>
        </div>
        <main className="mt-16 mx-auto max-w-7xl px-4 sm:mt-24">
          <div className="text-center">
            <h1 className="text-4xl tracking-tight font-extrabold text-white sm:text-5xl md:text-6xl">
              <span className="content-center">
                <div
                  style={{
                    marginLeft: "auto",
                    marginRight: "auto",
                    width: 200,
                  }}
                >
                  <LogoIcon fill="#fff" />
                </div>
              </span>
              <span className="block text-pink-500 xl:inline">Connecting Fans </span>
              <span className="block text-black xl:inline">To Creators</span>
            </h1>
            <p className="mt-3 max-w-md mx-auto text-base text-white sm:text-lg md:mt-5 md:text-xl md:max-w-3xl">
              The ultimate connection for creators &amp; fans to connect.
            </p>
            <div className="mt-5 max-w-md mx-auto sm:flex sm:justify-center md:mt-8">
              <div className="rounded-md shadow">
                <Link href="#">
                  <a
                    href="#"
                    className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10"
                  >
                    Fan Beta
                  </a>
                </Link>
              </div>
              <div className="mt-3 rounded-md shadow sm:mt-0 sm:ml-3">
                <Link href="register">
                  <a className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white hover:bg-gray-50 md:py-4 md:text-lg md:px-10">
                    Creator Sign-up
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export const ComingSoon: React.FC = () => {
  const [dimensions, setDimensions] = useState([0, 0]);

  useEffect(() => {
    const handleResize = () => setDimensions([window.innerWidth, window.innerHeight]);
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  return (
    <>
      <Content />
      <LavaLamp
        width={dimensions[0]}
        height={dimensions[1]}
        palette={palette}
        speed={2}
        scale={100}
        resolution={50}
        fadeInTime={150}
        animate={true}
      />
    </>
  );
};
