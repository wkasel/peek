import React from "react";
import { SectionDescription, SectionHeading, Subheading } from "./Typography";

export default {
  title: "Typography",
  component: SectionDescription,
};

export const _SectionDescription = () => (
  <SectionDescription>I'm a section description</SectionDescription>
);
export const _SectionHeading = () => <SectionHeading> I'm a section heading</SectionHeading>;
export const _SubHeading = () => <Subheading> I'm a sub heading </Subheading>;
