import React from "react";

export type IButton = React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
>;

export const Hamburger: React.FC<IButton> = ({
    className = "",
    children,
    ...rest
}) => {
    return (
        <div className="min-h-screen bg-gray-100 py-6 flex flex-col justify-center sm:py-12">
            <div className="relative py-3 sm:max-w-xl mx-auto">
                <nav x-data="{ open: false }">
                    <button className="text-gray-500 w-10 h-10 relative focus:outline-none bg-white :click:{open = !open}">
                        <span className="sr-only">Open main menu</span>
                        <div className="block w-5 absolute left-1/2 top-1/2   transform  -translate-x-1/2 -translate-y-1/2">
                            <span aria-hidden="true" className="block absolute h-0.5 w-5 bg-current transform transition duration-500 ease-in-out hover:{'rotate-45': open,' -translate-y-1.5': !open}" />
                            <span aria-hidden="true" className="block absolute  h-0.5 w-5 bg-current   transform transition duration-500 ease-in-out hover:{'opacity-0': open }" />
                            <span aria-hidden="true" className="block absolute  h-0.5 w-5 bg-current transform  transition duration-500 ease-in-out hover:{-rotate-45': open, ' translate-y-1.5': !open}" />
                        </div>
                    </button>
                </nav>
            </div>
        </div >

    );
};
