import React from "react";
import LogoIcon from "../icons/Logo";

export const Logo: React.FC = () => {
  return (
    <div className="flex justify-center pt-4">
      <LogoIcon stroke="#000" width="20rem" />
    </div>
  );
};
