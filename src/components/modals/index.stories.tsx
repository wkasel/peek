import React from "react";
import { Confirm } from "./confirm";

export default {
  title: "Confirm",
  component: Confirm,
};

export const Default = () => <Confirm />;
