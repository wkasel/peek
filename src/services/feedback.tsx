import { proxy } from "valtio";

export const store = proxy({
  alert: null as JSX.Element,
});
