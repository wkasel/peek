import Amplify, { API, Auth } from "aws-amplify";
import awsconfig from "../aws-exports";
import { proxy } from "valtio";
import * as feedback from "./feedback";
import { Success } from "../components/feedback/alerts/Success";
import { Error } from "../components/feedback/alerts/Error";

Amplify.configure(awsconfig);

interface Credentials {
  email: string;
  password: string;
}

export const store = proxy({
  cognitoUser: {
    username: "",
  },
  loading: false,
  email: "",
});

export const signUp = async (input: Credentials): Promise<any> => {
  try {
    store.loading = true;
    const user = await Auth.signUp({
      username: input.email,
      password: input.password,
      attributes: {
        email: input.email,
      },
    });
    store.email = input.email;
    return user;
  } catch (e) {
    feedback.store.alert = <Error>{e.message}</Error>;
    throw e;
  } finally {
    store.loading = false;
    feedback.store.alert = (
      <Success>Account creation successful! Check your email for a activation code.</Success>
    );
  }
};

export const signIn = async (input: Credentials): Promise<any> => {
  try {
    store.loading = true;
    const cognitoUser = await Auth.signIn(input.email, input.password);
    store.cognitoUser = cognitoUser;
    store.email = input.email;
    if (cognitoUser.challengeName === "NEW_PASSWORD_REQUIRED") {
      await Auth.completeNewPassword(cognitoUser, input.password);
    }
    try {
      // Get user by ID
      // Get profile image
      // Update profile image URL
    } catch (error) {
      // Update user
    }
  } catch (e) {
    feedback.store.alert = <Error>{e.message}</Error>;
    throw e;
  } finally {
    store.loading = false;
  }
};

export const confirmSignUp = async (code): Promise<any> => {
  try {
    store.loading = true;
    return await Auth.confirmSignUp(store.email, code);
  } catch (e) {
    feedback.store.alert = <Error>{e.message}</Error>;
    throw e;
  } finally {
    store.loading = false;
  }
};

export const confirmPasswordReset = async (input: Credentials, code): Promise<any> => {
  try {
    store.loading = true;
    return await Auth.forgotPasswordSubmit(input.email, code, input.password);
  } catch (e) {
    feedback.store.alert = <Error>{e.message}</Error>;
    throw e;
  } finally {
    store.loading = false;
  }
};

export const signOut = (): any => {
  return Auth.signOut();
};

export const changePassword = async (oldPassword, newPassword): Promise<any> => {
  try {
    store.loading = true;
    const me = await Auth.currentAuthenticatedUser();
    return await Auth.changePassword(me, oldPassword, newPassword);
  } catch (e) {
    feedback.store.alert = <Error>{e.message}</Error>;
    throw e;
  } finally {
    store.loading = false;
  }
};

export const resetPassword = async (email): Promise<any> => {
  try {
    store.loading = true;
    return await Auth.forgotPassword(email);
  } catch (e) {
    feedback.store.alert = <Error>{e.message}</Error>;
    throw e;
  } finally {
    store.loading = false;
  }
};
