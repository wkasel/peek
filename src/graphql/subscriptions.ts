/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser {
    onCreateUser {
      id
      verified
      profile {
        name {
          first
          middle
          last
        }
        email
        public
        socials {
          provider
          connected
        }
        tags
        online
        channels {
          id
          name
          description
        }
      }
      settings
      purchases
      subscriptions
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser {
    onUpdateUser {
      id
      verified
      profile {
        name {
          first
          middle
          last
        }
        email
        public
        socials {
          provider
          connected
        }
        tags
        online
        channels {
          id
          name
          description
        }
      }
      settings
      purchases
      subscriptions
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser {
    onDeleteUser {
      id
      verified
      profile {
        name {
          first
          middle
          last
        }
        email
        public
        socials {
          provider
          connected
        }
        tags
        online
        channels {
          id
          name
          description
        }
      }
      settings
      purchases
      subscriptions
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
