/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      verified
      profile {
        name {
          first
          middle
          last
        }
        email
        public
        socials {
          provider
          connected
        }
        tags
        online
        channels {
          id
          name
          description
        }
      }
      settings
      purchases
      subscriptions
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      verified
      profile {
        name {
          first
          middle
          last
        }
        email
        public
        socials {
          provider
          connected
        }
        tags
        online
        channels {
          id
          name
          description
        }
      }
      settings
      purchases
      subscriptions
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      verified
      profile {
        name {
          first
          middle
          last
        }
        email
        public
        socials {
          provider
          connected
        }
        tags
        online
        channels {
          id
          name
          description
        }
      }
      settings
      purchases
      subscriptions
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
