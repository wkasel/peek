import React from "react";
import { LavaLamp } from "src/components/lavalamp";
import { Container } from "src/components/layouts";

const Dashboard: React.FC = () => {
  return (
    <Container>
      <Background />
    </Container>
  );
};

export default Dashboard;

const palette = [
  [
    [198, 89, 198],
    [37, 159, 222],
  ],
  [
    [0, 159, 255],
    [151, 0, 255],
  ],
];

const Background: React.FC = () => {
  const [dimensions, setDimensions] = React.useState([0, 0]);

  React.useEffect(() => {
    const handleResize = () => setDimensions([window.innerWidth, window.innerHeight]);
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  return (
    <LavaLamp
      width={dimensions[0]}
      height={dimensions[1]}
      palette={palette}
      speed={2}
      scale={100}
      resolution={50}
      fadeInTime={150}
      animate={true}
    />
  );
};
