import React from "react";
import { Container } from "src/components/layouts";
import { Confirm } from "src/components/modals/confirm";

const Confirmation: React.FC = () => {
  return (
    <Container>
      <Confirm />
    </Container>
  );
};

export default Confirmation;
