import React from "react";
import { Container } from "src/components/layouts";
import { ComingSoon } from "src/components/sections/ComingSoon";

const Home: React.FC = () => {
  return (
    <Container>
      <ComingSoon />
    </Container>
  );
};

export default Home;
