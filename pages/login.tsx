import React from "react";
import { Login } from "src/components/authentication/Login";
import { Container } from "src/components/layouts";

const LoginPage: React.FC = () => {
  return (
    <Container>
      <Login />
    </Container>
  );
};

export default LoginPage;
