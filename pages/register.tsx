import React from "react";
import { Register } from "src/components/authentication/Register";
import { Container } from "src/components/layouts";

const Registration: React.FC = () => {
  return (
    <Container>
      <Register />
    </Container>
  );
};

export default Registration;
